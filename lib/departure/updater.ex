# Strategy found in
# https://stackoverflow.com/questions/32085258/how-to-run-some-code-every-few-hours-in-phoenix-framework

defmodule Departure.Updater do
  use GenServer
  use Timex

  def start_link do
    GenServer.start_link(__MODULE__, %{})
  end

  def init(state) do
    schedule_work()
    {:ok, state}
  end

  def handle_info(:work, state) do
    HTTPoison.start
    resp = HTTPoison.get! "http://developer.mbta.com/lib/gtrtfs/Departures.csv"

    datetime = Timex.local
    {a,date_str} = Timex.format(datetime, "%m-%d-%Y", :strftime)
    {a,time_str} = Timex.format(datetime, "%H:%M:%S", :strftime)
    {a,dow} = Timex.format(datetime, "%A", :strftime)

    trains = resp.body |> 
    String.split |>
    CSV.decode(headers: true) |>
    Enum.into([])

    Departure.Endpoint.broadcast("room", "update",
                                 %{:time => resp.status_code,
                                   :trains => trains,
                                   :date_str => date_str,
                                   :dow => dow,
                                   :time_str => time_str})

    schedule_work()
    {:noreply, state}
  end

  defp schedule_work() do
    Process.send_after(self(), :work, 1000)
  end
end
