defmodule Departure.PageController do
  use Departure.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
