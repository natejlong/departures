import { Socket }  from "phoenix"

export class RoomClient {
  constructor() {
    let roomChannel = this._setupRoomChannel()
  }
  _createSocket() {
    let socket = new Socket("/socket")
    socket.connect()
    return socket
  }

  _setupRoomChannel() {
    let socket = this._createSocket()
    let roomChannel = socket.channel("room", {})

    roomChannel.on('update', payload => {
      var departure_table,
        train,
        scheduled_date,
        train_scheduled_time,
        row;

      departure_table = `
        <tr>
          <th id='origin'>Origin</th>
          <th id='train-time'>Time</th>
          <th id='destination'>Destination</th>
          <th id='train'>Train #</th>
          <th id='track'>Track #</th>
          <th id='status'>Status</th>
        </tr>
      `;
      for(var i = 0; i < payload['trains'].length; i++) {
        train = payload['trains'][i];

        scheduled_date = new Date(0);
        scheduled_date.setUTCSeconds(parseInt(train['ScheduledTime']) + parseInt(train['Lateness']));

        train_scheduled_time = scheduled_date.toLocaleTimeString();
        if(train['Track'] == ''){
          train['Track'] = 'TBD';
        }

        row = "<tr>\n<td id='carrier'>" + 
         train['Origin'] + 
          "</td>\n<td id='train-scheduled-time'>" + 
          train_scheduled_time + 
          "</td>\n<td id='destination'>" + 
          train['Destination'] + 
          "</td>\n<td id='train-no'>" + 
          train['Trip'] + 
          "</td>\n<td id='track-no'>" + 
          train['Track'] + 
          "</td>\n<td id='status'>" + 
          train['Status'] + 
          "</td>";

        departure_table += row;
      }

      document.getElementById('departures').innerHTML = departure_table
      document.getElementById('dow').innerHTML = payload['dow']
      document.getElementById('current-time').innerHTML = payload['time_str']
      document.getElementById('date').innerHTML = payload['date_str']
    })

    roomChannel.join()
      .receive("ok", resp => { console.log("joined room") })
      .receive("error", reason => { console.log("error: " , reason) })
    return roomChannel
  }
}
