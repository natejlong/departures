defmodule Departure.RoomChannel do
  use Phoenix.Channel

  def join("room", _params, socket) do
    sendUpdate()
    {:ok, socket}
  end

  def sendUpdate() do
    Departure.Endpoint.broadcast("room", "update", %{})
  end
end
